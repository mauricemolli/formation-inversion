import numpy as np

###################################
# Define atoms to be considered
###################################

class Atom:

    def __init__(self, name, mass):

        self.name = name
        self.mass = mass

atoms = []
atoms.append(Atom('H', 1.))
atoms.append(Atom('He', 4.))
atoms.append(Atom('C', 12.))
atoms.append(Atom('N', 14.))
atoms.append(Atom('O', 16.))
atoms.append(Atom('P', 31.))
atoms.append(Atom('S', 32.))
atoms.append(Atom('Fe', 56.))
atoms.append(Atom('Mg', 24.))
atoms.append(Atom('Si', 28.))
atoms.append(Atom('Cl', 35.5))

###################################
# Define classes
###################################

class Species:

    def __init__(self, name, \
                 a_ice, \
                 mfr_disk, \
                 composition_mfr):

        self.name = name
        self.a_ice = a_ice
        self.mfr_disk = mfr_disk
        self.composition_mfr = composition_mfr
        self.real_chemical_profile = False

class ChemSpecies:

    def __init__(self, name, \
                 mfr_disk_gas, \
                 mfr_disk_solid, \
                 composition_mfr):

        self.name = name
        self.mfr_disk_gas = mfr_disk_gas
        self.mfr_disk_solid = mfr_disk_solid
        self.composition_mfr = composition_mfr
        self.real_chemical_profile = True

class Core:

    def __init__(self, species, a_accrete, mass):

        self.species = species
        self.a_accrete = a_accrete
        self.mass = mass

    def get_elemental_mass_fraction(self):

        # Calculate norm for currently active solid species, at a given disk radius
        species_norm = 0.
        
        for species in self.species:
            # If abundances from read-in chemical calculation
            if species.real_chemical_profile:
                species_norm = species_norm + \
                    species.mfr_disk_solid(self.a_accrete)
            # If Oeberg-like treatment
            else:
                if self.a_accrete > species.a_ice:
                    species_norm += species.mfr_disk

        # Calculate the elemental mass fraction of the accreted solids
        self.elemental_mass_fraction = {}
        for atom in atoms:
            self.elemental_mass_fraction[atom.name] = 0.
        
        for species in self.species:
            if species.real_chemical_profile:
                for atom in atoms:
                    try:
                        self.elemental_mass_fraction[atom.name] += \
                            species.mfr_disk_solid(self.a_accrete)/species_norm * \
                            species.composition_mfr[atom.name]
                    except:
                        pass
            else:
                if self.a_accrete > species.a_ice:
                    for atom in atoms:
                        try:
                            self.elemental_mass_fraction[atom.name] += \
                                species.mfr_disk/species_norm * \
                                species.composition_mfr[atom.name]
                        except:
                            pass

        return self.elemental_mass_fraction

    # Really: solids
class Planetesimal(Core):

    pass

    # Really: gas
class Envelope():

    def __init__(self, species, a_accrete, mass):

        self.species = species
        self.a_accrete = a_accrete
        self.mass = mass

    def get_elemental_mass_fraction(self):

        # Calculate norm for currently active gaseous species, at a given disk radius
        species_norm = 0.
        
        for species in self.species:
            # If abundances from read-in chemical calculation
            if species.real_chemical_profile:
                species_norm = species_norm + \
                    species.mfr_disk_gas(self.a_accrete)
            # If Oeberg-like treatment
            else:
                if self.a_accrete <= species.a_ice:
                    species_norm += species.mfr_disk

        # Calculate the elemental mass fraction of the accreted solids
        self.elemental_mass_fraction = {}
        for atom in atoms:
            self.elemental_mass_fraction[atom.name] = 0.

        for species in self.species:
            if species.real_chemical_profile:
                for atom in atoms:
                    try:
                        self.elemental_mass_fraction[atom.name] += \
                            species.mfr_disk_gas(self.a_accrete)/species_norm * \
                            species.composition_mfr[atom.name]
                    except:
                        pass
            else:
                if self.a_accrete <= species.a_ice:
                    for atom in atoms:
                        try:
                            self.elemental_mass_fraction[atom.name] += \
                                species.mfr_disk/species_norm * \
                                species.composition_mfr[atom.name]
                        except:
                            pass

        return self.elemental_mass_fraction

# Equal to what one would expect from a collapsing gas cloud (GI)
class GasAndSolids():

    def __init__(self, species, a_accrete):

        self.species = species
        self.a_accrete = a_accrete

    def get_elemental_mass_fraction(self):

        # Calculate norm for currently active gaseous species, at a given disk radius
        species_norm = 0.
        
        for species in self.species:
            # If abundances from read-in chemical calculation
            if species.real_chemical_profile:
                species_norm = species_norm + \
                    species.mfr_disk_gas(self.a_accrete) + \
                    species.mfr_disk_solid(self.a_accrete)
            # If Oeberg-like treatment
            else:
                species_norm += species.mfr_disk

        # Calculate the elemental mass fraction of the accreted solids
        self.elemental_mass_fraction = {}
        for atom in atoms:
            self.elemental_mass_fraction[atom.name] = 0.

        for species in self.species:
            if species.real_chemical_profile:
                for atom in atoms:
                    try:
                        self.elemental_mass_fraction[atom.name] += \
                            (species.mfr_disk_gas(self.a_accrete) + \
                            species.mfr_disk_solid(self.a_accrete))/species_norm * \
                            species.composition_mfr[atom.name]
                    except:
                        pass
            else:
                for atom in atoms:
                    try:
                        self.elemental_mass_fraction[atom.name] += \
                            species.mfr_disk/species_norm * \
                            species.composition_mfr[atom.name]
                    except:
                        pass

        return self.elemental_mass_fraction

    def get_elemental_number_fraction(self):

        mass_fractions = self.get_elemental_mass_fraction()

        self.elemental_number_fraction = {}
        for atom in atoms:
            self.elemental_number_fraction[atom.name] = 0.

        MMW = 0.
        for atom in atoms:
            MMW = MMW + mass_fractions[atom.name]/atom.mass
        MMW = 1./MMW
        
        for atom in atoms:
            self.elemental_number_fraction[atom.name] = \
                mass_fractions[atom.name] / \
                atom.mass * \
                MMW

        return self.elemental_number_fraction

class Planet():

    def __init__(self, core, planetesimal, envelope):

        self.core = core
        self.planetesimal = planetesimal
        self.envelope = envelope

    def get_elemental_mass_fraction(self, \
                                    fmix_core = 1., \
                                    fmix_planetesimal = 1.):

        self.core.get_elemental_mass_fraction()
        self.planetesimal.get_elemental_mass_fraction()
        self.envelope.get_elemental_mass_fraction()

        self.elemental_mass_fraction = {}
        for atom in atoms:
            self.elemental_mass_fraction[atom.name] = 0.

        for atom in atoms:
            self.elemental_mass_fraction[atom.name] += \
                fmix_core * self.core.mass * \
                    self.core.elemental_mass_fraction[atom.name] + \
                fmix_planetesimal * self.planetesimal.mass * \
                    self.planetesimal.elemental_mass_fraction[atom.name] + \
                self.envelope.mass * \
                    self.envelope.elemental_mass_fraction[atom.name]
            self.elemental_mass_fraction[atom.name] /= \
                (fmix_core * self.core.mass \
                 + fmix_planetesimal * self.planetesimal.mass \
                 + self.envelope.mass)
                
        return self.elemental_mass_fraction

    def get_elemental_number_fraction(self, \
                                    fmix_core = 1., \
                                    fmix_planetesimal = 1.):

        mass_fractions = self.get_elemental_mass_fraction(fmix_core, \
                                                          fmix_planetesimal)

        self.elemental_number_fraction = {}
        for atom in atoms:
            self.elemental_number_fraction[atom.name] = 0.

        MMW = 0.
        for atom in atoms:
            MMW = MMW + mass_fractions[atom.name]/atom.mass
        MMW = 1./MMW
        
        for atom in atoms:
            self.elemental_number_fraction[atom.name] = \
                mass_fractions[atom.name] / \
                atom.mass * \
                MMW

        return self.elemental_number_fraction

class DiskAnalysis:

    def __init__(self, species):

        self.species = species

    def get_planet_abundances(self, m_planet, \
                          a_acc_core, \
                          m_core, \
                          a_acc_planetesimal, \
                          m_planetesimal, \
                          a_acc_gas):

        m_gas = m_planet - m_core - m_planetesimal
        assert m_gas >= 0, 'Mcore + Mplanetesimal > Mplanet!'

        core = Core(self.species, a_acc_core, m_core)
        planetesimal = Planetesimal(self.species, a_acc_planetesimal, m_planetesimal)
        envelope = Envelope(self.species, a_acc_gas, m_gas)

        planet = Planet(core, planetesimal, envelope)

        planet_abund = planet.get_elemental_number_fraction()
        C = planet_abund['C']
        O = planet_abund['O']
        N = planet_abund['N']
        P = planet_abund['P']

        return np.array([C, O, N, P])

    def get_disk_abundances(self, a_acc):

        disk = GasAndSolids(self.species, a_acc)

        abund = disk.get_elemental_number_fraction()
        C = abund['C']
        O = abund['O']
        N = abund['N']
        P = abund['P']

        return np.array([C, O, N, P]), abund

    def get_disk_abundance_characteristics(self):

        a_acc = np.logspace(0.,2,100)

        ####################
        # Solids
        ####################

        m_planet = 318.
        a_acc_core = 4.
        m_core = 0.
        a_acc_planetesimal = 4.
        m_planetesimal = 318.
        a_acc_gas = 4.

        a_acc_planetesimal = a_acc
        COs_solid = np.zeros_like(a_acc)

        for i in range(len(a_acc_planetesimal)):
            print(i)
            nfrs = self.get_planet_abundances(m_planet, \
                                              a_acc_core, \
                                              m_core, \
                                              a_acc_planetesimal[i], \
                                              m_planetesimal, \
                                              a_acc_gas)

            COs_solid[i] = nfrs[0]/nfrs[1]

        ####################
        # Gas
        ####################

        m_planet = 318.
        a_acc_core = 4.
        m_core = 0.
        a_acc_planetesimal = 4.
        m_planetesimal = 0.
        a_acc_gas = 4.

        a_acc_gas = a_acc
        COs_gas = np.zeros_like(a_acc)

        for i in range(len(a_acc_gas)):
            print(i)
            nfrs = self.get_planet_abundances(m_planet, \
                                              a_acc_core, \
                                              m_core, \
                                              a_acc_planetesimal, \
                                              m_planetesimal, \
                                              a_acc_gas[i])

            COs_gas[i] = nfrs[0]/nfrs[1]

        ####################
        # Disk
        ####################

        COs_disk = np.zeros_like(a_acc)

        for i in range(len(a_acc_gas)):
            print(i)
            nfrs, disk_nfr = self.get_disk_abundances(a_acc[i])
            COs_disk[i] = nfrs[0]/nfrs[1]

        return a_acc, COs_solid, COs_gas, COs_disk
