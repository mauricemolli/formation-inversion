import os
os.environ["OMP_NUM_THREADS"] = "1"

import numpy as np
import sys
import pymultinest
import json
from scipy.stats import norm
import nat_cst as nc

from abundance_classes import *
from disk_abund import *

# Define function to make observables
parameters = []
parameters.append('M_planet')
parameters.append('a_planetesimals')
parameters.append('M_planetesimals')
parameters.append('a_gas')

observables = []
observables.append('C/H')
observables.append('O/H')
observables.append('N/H')
observables.append('P/H')

def get_planet_abundances(m_planet, \
                          a_acc_planetesimal, \
                          m_planetesimal, \
                          a_acc_gas):

    m_gas = m_planet - m_planetesimal
    assert m_gas > 0, 'Mplanetesimal > Mplanet!'

    core = Core(species, 2., 0.)
    planetesimal = Planetesimal(species, a_acc_planetesimal, m_planetesimal)
    envelope = Envelope(species, a_acc_gas, m_gas)

         # "core not used", solids,     gas
    planet = Planet(core, planetesimal, envelope)
    
    planet_abund = planet.get_elemental_number_fraction()
    CoH = planet_abund['C']/planet_abund['H']
    OoH = planet_abund['O']/planet_abund['H']
    NoH = planet_abund['N']/planet_abund['H']
    PoH = planet_abund['P']/planet_abund['H']

    return np.log10(np.array([CoH, OoH]))

# Load abundances to be inverted
retrieval_name = 'test_hr8799e'
relative_log_ab_obs = np.load('mu_CoHOoH.npy')
covariance = np.load('covarianceCoHOoH.npy')
inv_covariance = np.linalg.inv(covariance)

# Define prior
def prior(cube, ndim, nparams):

    logg = 4. + 0.49 * norm.ppf(cube[0])
    mass = 1e1**logg/nc.G*(1.12*nc.r_jup_mean)**2./nc.m_earth
    acc_planetesimal = 100. * cube[1]
    m_planetesimal = 1000*cube[2]
    acc_gas = 100. * cube[3]

    cube[0] = mass
    cube[1] = acc_planetesimal
    cube[2] = m_planetesimal
    cube[3] = acc_gas
    return

# define loglike
def loglike(cube, ndim, nparams):

    mass = cube[0]
    acc_planetesimal = cube[1]
    m_planetesimal = cube[2]
    acc_gas = cube[3]

    if mass < m_planetesimal:
        return -np.inf

    relative_log_ab = get_planet_abundances(mass, \
                          acc_planetesimal, \
                          m_planetesimal, \
                          acc_gas)

    diff = (relative_log_ab-relative_log_ab_obs)

    return -np.dot(diff, \
                     inv_covariance.dot(diff))/2.

n_params = 4

json.dump(parameters, open('out_PMN/'+retrieval_name+'_params.json', 'w'))

pymultinest.run(loglike, prior, n_params, \
                outputfiles_basename='out_PMN/'+retrieval_name+'_', \
                resume = False, verbose = True, \
                n_live_points = 4000)






























'''
a_accrete_au = 30.
m_planet_mearth = 10.
core = Core(species, a_accrete_au, m_planet_mearth)

a_accrete_au = 15.
m_planet_mearth = 2.
planetesimal = Planetesimal(species, a_accrete_au, m_planet_mearth)

a_accrete_au = 15.
m_planet_mearth = 2.
envelope = Envelope(species, a_accrete_au, m_planet_mearth)

planet = Planet(core, planetesimal, envelope)
planet_abund = planet.get_elemental_number_fraction()
s = 0.
for f in planet_abund.keys():
    s += planet_abund[f]
print(s)
print(planet_abund)
'''
