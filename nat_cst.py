import numpy as np
import pylab as plt

# Everything is in cgs!

c = 2.99792458e10
G = 6.67384e-8
kB = 1.3806488e-16
sigma = 5.670373e-5
bar = 1e6
atm = 1.01325e6
losch = 2.68676e19
h = 6.62606957e-27
r_sun = 6.955e10
r_jup=7.1492e9
r_jup_mean=6.9911e9
m_jup = 1.89813e30
m_sun = 1.9891e33
AU = 1.49597871e13
pc = 3.08567758e18
amu = 1.66053892e-24
nA = 6.0221413e23
R = 8.3144598
m_earth = 5.9722e27
r_earth = 637813660.
m_elec = 9.10938356e-28
e = 4.80320425e-10

def b(T,nu):
    retVal = 2.*h*nu**3./c**2.
    retVal = retVal / (np.exp(h*nu/kB/T)-1.)
    return retVal

def dbdT(T,nu):
    retVal = 2.*h*nu**3./c**2.
    retVal = retVal / (np.exp(h*nu/kB/T)-1.)**2.
    retVal = retVal * np.exp(h*nu/kB/T) * h*nu/kB/T**2.
    return retVal

def guillot_day(P,kappa_IR,gamma,grav,T_int,T_irr):

    tau = P*1e6*kappa_IR/grav
    T = (0.75*T_int**4.*(2./3.+tau) + \
      0.75*T_irr**4./2.*(2./3.+1./gamma/3.**0.5+ \
                         (gamma/3.**0.5-1./3.**0.5/gamma)*np.exp(-gamma*tau*3.**0.5)))**0.25
    return T

def guillot_global(P,kappa_IR,gamma,grav,T_int,T_equ):

    tau = P*1e6*kappa_IR/grav
    T_irr = T_equ*np.sqrt(2.)
    T = (0.75*T_int**4.*(2./3.+tau) + \
      0.75*T_irr**4./4.*(2./3.+1./gamma/3.**0.5+ \
                         (gamma/3.**0.5-1./3.**0.5/gamma)*np.exp(-gamma*tau*3.**0.5)))**0.25
    return T

def get_dist(T_irr,dist,T_star,R_star,mode,mode_what):

    mu_star = 0.
    angle_use = False
    if ((mode != 'p') & (mode != 'd')) :
        mu_star = float(mode)
        angle_use = True

    if mode_what == 'temp':
        if angle_use:
            T_irr = ((R_star*r_sun/(dist*AU))**2.*T_star**4.*mu_star)**0.25
        elif mode == 'p':
            T_irr = ((R_star*r_sun/(dist*AU))**2.*T_star**4./4.)**0.25
        else:        
            T_irr = ((R_star*r_sun/(dist*AU))**2.*T_star**4./2.)**0.25
        return T_irr
    elif mode_what == 'dist':
        if angle_use:
            dist = np.sqrt((R_star*r_sun)**2.*(T_star/T_irr)**4.*mu_star)/AU
        elif mode == 'p':
            dist = np.sqrt((R_star*r_sun)**2.*(T_star/T_irr)**4./4.)/AU
        else:
            dist = np.sqrt((R_star*r_sun)**2.*(T_star/T_irr)**4./2.)/AU
        return dist

def setup_atmo(ax=None):
    if ax == None:
        plt.yscale('log')
        plt.ylim([1e3,1e-6])
    else:
        ax.set_yscale('log')
        ax.set_ylim([1e3,1e-6])
    return


logs_g = np.array([12.,10.93])

logs_met = np.array([1.05,1.38,2.7,8.43,7.83,8.69,4.56,7.93,6.24,7.6,6.45,7.51,5.41, \
            7.12, 5.5, 6.4, 5.03, 6.34, 3.15, 4.95, 3.93, 5.64, 5.43, 7.5, \
            4.99, 6.22, 4.19, 4.56, 3.04, 3.65, 3.25, 2.52, 2.87, 2.21, 2.58, \
            1.46, 1.88])

def calc_met(f):
    return np.log10( (f/(np.sum(1e1**logs_g)+f*np.sum(1e1**logs_met))) / \
                     (1./(np.sum(1e1**logs_g)+np.sum(1e1**logs_met))) )


def box_car_conv(array,points):

    res = np.zeros_like(array)
    len_arr = len(array)
    for i in range(len(array)):
        if (i-points/2 >= 0) and (i+points/2 <= len_arr+1):
            smooth_val = array[i-points/2:i+points/2]
            res[i] = np.sum(smooth_val)/len(smooth_val)
        elif (i+points/2 > len_arr+1):
            len_use = len_arr+1-i
            smooth_val = array[i-len_use:i+len_use]
            res[i] = np.sum(smooth_val)/len(smooth_val)
        elif i-points/2 < 0:
            smooth_val = array[:max(2*i,1)]
            res[i] = np.sum(smooth_val)/len(smooth_val)
    return res

def read_abunds(path):

    f = open(path)
    header = f.readlines()[0][:-1]
    f.close()
    ret = {}

    dat = np.genfromtxt(path)
    ret['P'] = dat[:,0]
    ret['T'] = dat[:,1]
    ret['rho'] = dat[:,2]

    for i in range(int((len(header)-21)/22)):
        if i % 2 == 0:
            #print(len(header[21+(i)*22:21+(i+1)*22]),'mfr:',header[21+(i)*22:21+(i+1)*22],'number',int(header[21+(i)*22:21+(i+1)*22][0:3]))
            name = header[21+(i)*22:21+(i+1)*22][3:].replace(' ','')
            number = int(header[21+(i)*22:21+(i+1)*22][0:3])
            #print(name)
            ret['m'+name] = dat[:,number]
        else:
            #print(len(header[21+(i)*22:21+(i+1)*22]),'nfr:',header[21+(i)*22:21+(i+1)*22],'number',int(header[21+(i)*22:21+(i+1)*22][0:3]))
            name = header[21+(i)*22:21+(i+1)*22][3:].replace(' ','')
            number = int(header[21+(i)*22:21+(i+1)*22][0:3])
            #print(name)
            ret['n'+name] = dat[:,number]

    return ret

def running_mean(x, N):
    cumsum = np.cumsum(np.insert(x, 0, 0)) 
    return (cumsum[N:] - cumsum[:-N]) / float(N)
