import numpy as np
import pylab as plt
import json
import sys
import os

from nice_corner import nice_corner

os.system('ls out_PMN/*post_equal* > tmp')
f = open('tmp')
prefix = f.readlines()[0][:-1].split('post_equal')[0]
f.close()
os.system('rm tmp')

parameters = json.load(open(prefix + 'params.json'))
for i in range(len(parameters)):
    parameters[i] = parameters[i].replace('_',' ')
    print(parameters[i])

parameters[0] = r'$M_{\rm P}$ (M$_{\rm Jup}$)'
parameters[1] = r'$a_{\rm solid}$ (arb. units)'
parameters[2] = r'$M_{\rm solid}$ (M$_\oplus$)'
parameters[3] = r'$a_{\rm gas}$ (arb. units)'

samples = np.genfromtxt(prefix + 'post_equal_weights.dat')[:,:-1]
output_file = 'corner.pdf'

import nat_cst as nc
samples[:,0] = samples[:,0]*nc.m_earth/nc.m_jup

parameter_ranges = []
parameter_ranges.append((0,50.))
parameter_ranges.append((0,75))
parameter_ranges.append((0.,1000.))
parameter_ranges.append((0,75))
#parameter_ranges.append((0.,40.))
#parameter_ranges.append((0.,30.))

#for i_p in range(len(parameters)):
#    parameter_ranges.append(None)

parameter_plot_indices = [0, 1, 2, 3]

true_values = None #[318, 30, 10, 15, 10, 5.2]

max_val_ratio = 1.5

nice_corner(samples, \
            parameters, \
            output_file, \
            N_samples = len(samples), \
            parameter_plot_indices = parameter_plot_indices, \
            max_val_ratio = max_val_ratio, \
            parameter_ranges = parameter_ranges, \
            true_values = true_values)
