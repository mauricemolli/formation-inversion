import numpy as np
from abundance_classes import *

################################################
# Define disk species, as in Table 2 in paper
################################################

C_depletion = 0.25

species = []

# H2/He background
name = 'Background'
a_ice = 1000.
mfr_disk = 0.9879406547619047
composition_mfr = {}
composition_mfr['H'] = 0.75
composition_mfr['He'] = 0.25
species.append(Species(name, \
                       a_ice, \
                       mfr_disk, \
                       composition_mfr))

# H2O
name = 'H2O'
a_ice = 2.3
mfr_disk = 18.*1.6e-4/(1.4) - \
    C_depletion * 18. * 2.6e-4/2./1.4
composition_mfr = {}
composition_mfr['H'] = 2./18.
composition_mfr['O'] = 16./18.
species.append(Species(name, \
                       a_ice, \
                       mfr_disk, \
                       composition_mfr))

# CO2
name = 'CO2'
a_ice = 8.0
mfr_disk = 44.*4e-5/(1.4)
composition_mfr = {}
composition_mfr['C'] = 12./44.
composition_mfr['O'] = 32./44.
species.append(Species(name, \
                       a_ice, \
                       mfr_disk, \
                       composition_mfr))

# CO
name = 'CO'
a_ice = 35.
mfr_disk = 28.*8e-5/(1.4) + \
    C_depletion * 28. * 2.6e-4/2./1.4
composition_mfr = {}
composition_mfr['C'] = 12./28.
composition_mfr['O'] = 16./28.
species.append(Species(name, \
                       a_ice, \
                       mfr_disk, \
                       composition_mfr))

# N2
name = 'N2'
a_ice = 73.
mfr_disk = 28.*3e-5/(1.4)
composition_mfr = {}
composition_mfr['N'] = 1.
species.append(Species(name, \
                       a_ice, \
                       mfr_disk, \
                       composition_mfr))

# NH3
name = 'NH3'
a_ice = 6.5
mfr_disk = 17.*7e-6/(1.4)
composition_mfr = {}
composition_mfr['N'] = 14./17.
composition_mfr['H'] = 3./17.
species.append(Species(name, \
                       a_ice, \
                       mfr_disk, \
                       composition_mfr))

# Grains
name = 'Refractories'
a_ice = 0.
# Silicate MgSiO3 VMR = Water VMR
# All iron
# Half of all C
# All P and S
mfr_disk = (100.*1.6e-4/3. + \
            56.*3.2e-5 + \
            12.*2.6e-4/2.*(1.-C_depletion) + \
            31.*2.5e-7 + \
            32.*1.3e-5)/ \
            (1.4)

# Silicates O content equal to H2O oxygen nfr, as in Oeberg+19
# Fe from Asplund+09
# C from Asplund+09, but only half, as in Oeberg+19
# P from Asplund+09
# S from Asplund+09

composition_mfr = {}
composition_mfr['Fe'] = 56.*3.2e-5/(mfr_disk*(1.4))
composition_mfr['Mg'] = 24.*1.6e-4/3./(mfr_disk*(1.4))
composition_mfr['Si'] = 28.*1.6e-4/3./(mfr_disk*(1.4))
composition_mfr['O'] = 48.*1.6e-4/3./(mfr_disk*(1.4))
composition_mfr['C'] = 12.*2.6e-4/2./(mfr_disk*(1.4))*(1.-C_depletion)
composition_mfr['P'] = 31.*2.5e-7/(mfr_disk*(1.4))
composition_mfr['S'] = 32.*1.3e-5/(mfr_disk*(1.4))
species.append(Species(name, \
                       a_ice, \
                       mfr_disk, \
                       composition_mfr))
