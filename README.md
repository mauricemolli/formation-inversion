# Formation Inversion

Base formation inversion setup of the publication "Interpreting the atmospheric composition of exoplanets: sensitivity to planet formation assumptions", by Mollière et al. (2022).

For a video tutorial on how to use this code, please see: https://keeper.mpdl.mpg.de/f/f6be39686fe34450be71/