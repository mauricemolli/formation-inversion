import numpy as np
import pylab as plt
import copy  as cp

def make_abundances(FEH, CO):

    elem_names = [ 'H', 'He', 'C', 'N', 'O', 'Na', \
                       'Mg', 'Al', 'Si', 'P', 'S', 'Cl', \
                       'K', 'Ca', 'Ti', 'V', 'Fe', 'Ni']
    elem_abunds = [0.9207539305, 0.0783688694, 0.0002478241, \
                       6.22506056949881E-05, 0.0004509658, 1.60008694353205E-06, 3.66558742055362E-05, \
                       0.000002595, 0.000029795, 2.36670201997668E-07, 1.2137900734604E-05, 2.91167958499589E-07, \
                       9.86605611925677E-08, 2.01439011429255E-06, 8.20622804366359E-08, 7.83688694089992E-09, \
                       2.91167958499589E-05, 1.52807116806281E-06]

    ab_out = {}
    for i, name in enumerate(elem_names):
        if name not in ['H', 'He']:
            ab_out[name] = elem_abunds[i]*1e1**FEH
        else:
            ab_out[name] = elem_abunds[i]

    ab_out['O'] = ab_out['C']/CO

    return ab_out

FEHs = np.random.normal(size=10000)*0.27+0.48
COs  = np.random.normal(size=10000)*0.075+0.60*0.5208333333333333/0.55

CoHnOoHs = []
for i in range(len(COs)):
    ab_out = make_abundances(FEHs[i], COs[i])
    CoHnOoHs.append([ab_out['C']/ab_out['H'], ab_out['O']/ab_out['H']])


CoHnOoHs = np.log10(np.array(CoHnOoHs))
plt.scatter(CoHnOoHs[:,0], CoHnOoHs[:,1], alpha=0.3)
print(np.mean(1e1**(CoHnOoHs[:,0]-CoHnOoHs[:,1])))

print(np.mean(CoHnOoHs[:,0]))
print(np.mean(CoHnOoHs[:,1]))

print(np.sqrt(np.cov(CoHnOoHs, rowvar=False)))
np.save('covarianceCoHOoH.npy', np.cov(CoHnOoHs, rowvar=False))

np.save('mu_CoHOoH.npy', np.array([np.mean(CoHnOoHs[:,0]), np.mean(CoHnOoHs[:,1])]))

plt.show()
