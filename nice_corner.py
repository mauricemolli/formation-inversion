import numpy as np
import seaborn as sns
import pylab as plt
from matplotlib import ticker

def nice_corner(samples, \
                parameter_names, \
                output_file, \
                N_samples = None, \
                parameter_ranges = None, \
                parameter_plot_indices = None, \
                true_values = None, \
                max_val_ratio = None):

    font = {'family' : 'serif',
        'weight' : 'normal',
            'size'   : int(19*5./len(parameter_plot_indices))}

    plt.rc('font', **font)
    plt.rc('text', usetex=True)


    if N_samples != None:
        S = N_samples
    else:
        S = len(samples)

    try:
        if parameter_plot_indices == None:
            parameter_plot_indices = np.linspace(0, len(parameter_names)-1, \
                                        len(parameter_names)-1).astype('int')
    except:
        pass
                                        
    if max_val_ratio == None:
        max_val_ratio = 5.

    data_list = []
    labels_list = []
    range_list = []
    
    for i in parameter_plot_indices:
        
        data_list.append(samples[len(samples)-S:,i])
        labels_list.append(parameter_names[i])

        try:
            if parameter_ranges[i] == None:
                range_mean = np.mean(samples[len(samples)-S:,i])
                range_std = np.std(samples[len(samples)-S:,i])
                range_take = (range_mean-4*range_std, range_mean+4*range_std)
                range_list.append(range_take)
            else:
                range_list.append(parameter_ranges[i])
        except:
            range_mean = np.mean(samples[len(samples)-S:,i])
            range_std = np.std(samples[len(samples)-S:,i])
            range_take = (range_mean-4*range_std, range_mean+4*range_std)
            range_list.append(range_take)
            
    try:
        truths_list = []
        for i in parameter_plot_indices:
            truths_list.append(true_values[i])
    except:
        pass

    dimensions = len(parameter_plot_indices)

    # Set up the matplotlib figure
    f, axes = plt.subplots(dimensions, dimensions, figsize=(13, 13), \
                           sharex=False, sharey=False)
    i_col = 0
    i_lin = 0

    try:
        ax_array = axes.flat
    except:
        ax_array = [plt.gca()]
    #print(len(axes).flat)
    for ax in ax_array:

        #print(i_col, i_lin, dimensions, len(axes.flat))
        if i_col < i_lin:
            x = cp.copy(data_list[i_col])
            y = cp.copy(data_list[i_lin])
            indexx = (x >= range_list[i_col][0]) & \
                (x <= range_list[i_col][1])
            indexy = (y >= range_list[i_lin][0]) & \
                (y <= range_list[i_lin][1])

            x = x[indexx & indexy]
            y = y[indexx & indexy]

            if 'a ' in labels_list[i_col]:
                x = np.array(x)
            if 'a ' in labels_list[i_lin]:
                y = np.array(y)

            gridsize = 30
            ax.hexbin(x, y, cmap='bone_r', gridsize=gridsize, \
                          vmax = int(max_val_ratio*S/gridsize**2.), \
                          rasterized=True)

            ax.set_xlim([range_list[i_col][0], range_list[i_col][1]])
            ax.set_ylim([range_list[i_lin][0], range_list[i_lin][1]])

            try:
                ax.axhline(truths_list[i_lin], color = 'red', \
                               linestyle = '--', linewidth = 2.5)
                ax.axvline(truths_list[i_col], color = 'red', \
                               linestyle = '--', linewidth = 2.5)
            except:
                pass
            
            if i_col > 0:
                ax.get_yaxis().set_visible(False)

        elif i_col == i_lin:
            
            med = np.median(data_list[i_col])
            up = str(np.round(np.percentile(data_list[i_col], 84) - med,2))
            do = str(np.round(med - np.percentile(data_list[i_col], 16),2))
            med = str(np.round(med,2))
            med = med.split('.')[0]+'.'+med.split('.')[1].ljust(2, '0')
            up = up.split('.')[0]+'.'+up.split('.')[1].ljust(2, '0')
            do = do.split('.')[0]+'.'+do.split('.')[1].ljust(2, '0')

            if not 'a_' in labels_list[i_col]:
                ax.set_title(med+r'$^{+'+up+'}_{-'+do+'}$', \
                             fontdict=font, fontsize = \
                             int(18*5./len(parameter_plot_indices)))
            
            import copy as cp
            use_data = cp.copy(data_list[i_col])
            index = (use_data >= range_list[i_col][0]) & \
                (use_data <= range_list[i_col][1])
            use_data = use_data[index]
            if not 'a_' in labels_list[i_col]:
                #sns.distplot(use_data, bins=30, kde=False, \
                #             rug=False, ax=ax, color = 'gray')
                ax.hist(use_data, 
                         bins=30, 
                         range=[range_list[i_col][0], range_list[i_col][1]], 
                         density=True, 
                         weights=None, 
                         cumulative=False, 
                         bottom=None, 
                         histtype='stepfilled',
                         align='mid',
                         orientation='vertical',
                         rwidth=None,
                         log=False,
                         color='gray',
                         label=None,
                         stacked=False, 
                         alpha = 0.5)
            else:
                ax.hist(np.array(use_data), 
                         bins=30, 
                         range=[range_list[i_col][0], range_list[i_col][1]], 
                         density=True, 
                         weights=None, 
                         cumulative=False, 
                         bottom=None, 
                         histtype='stepfilled',
                         align='mid',
                         orientation='vertical',
                         rwidth=None,
                         log=False,
                         color='gray',
                         label=None,
                         stacked=False, 
                         alpha = 0.5)
                #sns.distplot(np.array(use_data), bins=30, kde=False, \
                #             rug=False, ax=ax, color = 'gray')
            ax.set_xlim([range_list[i_col][0], range_list[i_col][1]])
            ax.get_yaxis().set_visible(False)
            try:
                ax.axvline(truths_list[i_col], color = 'red', \
                                   linestyle = '--', linewidth = 2.5)
            except:
                pass

            '''
            ax.axvline(float(med)+float(up), color = 'black', \
                           linestyle = ':', linewidth=1.0)
            ax.axvline(float(med), color = 'black', \
                           linestyle = ':', linewidth=1.0)
            ax.axvline(float(med)-float(do), color = 'black', \
                           linestyle = ':', linewidth=1.0)
            '''
        else:
            ax.axis('off')

        labels = ax.yaxis.get_ticklabels(which='both')
        #print(labels)
        #labels[-1].set_visible(False)

        i_col += 1
        if i_col%dimensions == 0:
            i_col = 0
            i_lin += 1

    for i_col in range(dimensions):
        for i_lin in range(dimensions):
            try:
                plt.sca(axes[i_lin, i_col])
            except:
                pass
            range_use = np.linspace(range_list[i_col][0], \
                                        range_list[i_col][1], 5)[1:-1]

            labels_use = []
            for r in range_use:
                labels_use.append(str(np.round(r,1)))
            plt.xticks(range_use[::2], labels_use[::2])
            if 'a_' in labels_list[i_col]:
                if i_lin >= i_col:
                #plt.xticks((2,4,6,20,26), (r'H$_2$O', r'NH$_3$', r'CO$_2$', r'CO', r'N$_2$'), \
                #           rotation = 45)
                    ices = [2.3,6.5,8.,35.,73.]
                    for ice in ices:
                        plt.axvline(ice, color = 'black', linewidth=2, linestyle='--')

            if 'a_' in labels_list[i_lin]:
                if i_lin > i_col:
                #plt.xticks((2,4,6,20,26), (r'H$_2$O', r'NH$_3$', r'CO$_2$', r'CO', r'N$_2$'), \
                #           rotation = 45)
                    ices = [2.3,6.5,8.,35.,73.]
                    for ice in ices:
                        plt.axhline(ice, color = 'black', linewidth=2, linestyle='--')
            if 'a_' in labels_list[i_col]:
                ices = [2.3,6.5,8.,35.,73.]
                names = [r'H$_2$O',r'NH$_3$',r'CO$_2$',r'CO',r'N$_2$']
                plt.xticks(ices, names, rotation='vertical', fontsize = 10)
                plt.tick_params(axis='x', length = 0.)
            plt.xlabel(labels_list[i_col])

    for i_lin in range(dimensions):
        try:
            plt.sca(axes[i_lin,0])
        except:
            pass
        plt.ylabel(labels_list[i_lin])
        if 'a_' in labels_list[i_lin]:
            ices = [2.3,6.5,8.,35.,73.]
            names = [r'H$_2$O',r'NH$_3$',r'CO$_2$',r'CO',r'N$_2$']
            plt.yticks(ices, names, fontsize = 10)
            plt.tick_params(axis='y', length = 0.)
            
    axes[-1, 1].text(0.75, -0.05, 'icelines', va = 'center', ha = 'center', fontsize=10., transform=axes[-1,1].transAxes)
    axes[-1, 3].text(0.75, -0.05, 'icelines', va = 'center', ha = 'center', fontsize=10., transform=axes[-1,3].transAxes)

    axes[1, 0].text(-0.05,0.75, 'icelines', va = 'center', ha = 'center', fontsize=10., transform=axes[1,0].transAxes, rotation = 90)
    axes[-1, 0].text(-0.05,0.75, 'icelines', va = 'center', ha = 'center', fontsize=10., transform=axes[-1,0].transAxes, rotation = 90)
            
    plt.subplots_adjust(wspace = 0, hspace = 0)
    if dimensions == 1:
        plt.tight_layout(h_pad=0, w_pad=0)
    plt.savefig(output_file)
    #plt.show()
    #plt.clf()
